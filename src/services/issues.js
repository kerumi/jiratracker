import { createRequest } from './config';
import { getCurrentUser } from '../utils/user';

export function getProjectIssues(key) {
  const { host, email, password } = getCurrentUser();
  const request = createRequest({ host, email, password });
  return request.get(`/rest/api/3/search?jql=project="${key}"`);
}

export function getProjectIssueLog(key) {
  const { host, email, password } = getCurrentUser();
  const request = createRequest({ host, email, password });
  return request.get(
    `/rest/api/3/issue/${key}/changelog?maxResults=50&startAt=0`
  );
}
