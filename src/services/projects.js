import { createRequest } from './config';
import { getCurrentUser } from '../utils/user';

export function getProjects() {
  const { host, email, password } = getCurrentUser();
  const request = createRequest({ host, email, password });
  return request.get('/rest/api/3/project');
}
