import { authenticate } from '../../services/auth';

let user = JSON.parse(localStorage.getItem('user'));
const state = {
  loading: false,
  error: '',
  loggedIn: user ? true : false,
  currentUser: user ? user : {}
};
const actions = {
  login({ commit }, { host, email, password }) {
    commit('onLogin');
    authenticate(host, email, password).then(
      res => commit('onSuccess', { ...res.data, host, password }),
      error => commit('onFailure', error)
    );
  },
  logout({ commit }) {
    commit('onLogout');
  }
};

const mutations = {
  onLogin(state) {
    state.loading = true;
  },
  onLogout(state) {
    localStorage.removeItem('user');
    state.currentUser = {};
    state.loggedIn = false;
  },
  onSuccess(state, result) {
    const user = JSON.stringify(result);
    state.loading = false;
    localStorage.setItem('user', user);
    state.currentUser = result;
    state.loggedIn = true;
  },
  onFailure(state, error) {
    state.loading = false;
    state.loggedIn = false;
    state.error = error.message;
  }
};

export const auth = {
  namespaced: true,
  state,
  actions,
  mutations
};
