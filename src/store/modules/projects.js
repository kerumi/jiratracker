import { getProjects } from '../../services/projects';

const state = {
  loading: false,
  error: '',
  projects: []
};

const actions = {
  importProjects({ commit }) {
    commit('onImport');
    getProjects().then(
      res => commit('onImportSuccess', res.data),
      error => commit('onImportFailure', error)
    );
  }
};

const mutations = {
  onImport(state) {
    state.loading = true;
  },
  onImportSuccess(state, projects) {
    state.projects = projects;
    state.loading = false;
  },
  onImportFailure(state, error) {
    state.loading = false;
    state.error = error.message;
  }
};

export const projects = {
  namespaced: true,
  state,
  actions,
  mutations
};
