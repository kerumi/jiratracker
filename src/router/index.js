import Vue from 'vue';
import Router from 'vue-router';

import Landing from '@/pages/Landing';
import Dashboard from '@/pages/Dashboard';
import ProjectLog from '@/pages/ProjectLog';
import NotFound from '@/pages/NotFound';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: '/jiratracker/',
  routes: [{
      path: '/',
      name: 'Landing',
      component: Landing,
      meta: {
        guest: true
      }
    },
    {
      path: '/home',
      name: 'Dashboard',
      component: Dashboard,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/project/:keyId',
      name: 'ProjectLog',
      component: ProjectLog,
      props: true,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '*',
      component: NotFound
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('user') == null) {
      next({
        path: '/',
        params: {
          nextUrl: to.fullPath
        }
      });
    } else {
      next();
    }
  } else if (to.matched.some(record => record.meta.guest)) {
    if (localStorage.getItem('user') == null) {
      next();
    } else {
      next({
        name: 'Dashboard'
      });
    }
  } else {
    next();
  }
});

export default router;
