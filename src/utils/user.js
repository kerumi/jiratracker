import { isNil } from 'ramda';

const parseUser = user => {
  const parsed = JSON.parse(user);
  return { ...parsed, email: parsed.emailAddress };
};

export const getCurrentUser = () => {
  const user = localStorage.getItem('user');
  return !isNil(user) ? parseUser(user) : {};
};
