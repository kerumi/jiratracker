import xlsx from 'xlsx';

export function generateLog(data) {
  let wb = xlsx.utils.book_new();
  wb.Props = {
    Title: "JiraTracker Log",
    Subject: "Project Log",
    Author: "KelviNosse",
    CreatedDate: new Date()
  };
  wb.SheetNames.push("Log Report");
  let ws = xlsx.utils.json_to_sheet(data);
  wb.Sheets["Log Report"] = ws;
  let wbout = xlsx.write(wb, {
    bookType: 'xlsx',
    type: 'binary'
  });

  function toArrayBuffer(s) {
    let buf = new ArrayBuffer(s.length);
    let view = new Uint8Array(buf);
    for (let i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
  }

  return new Blob([toArrayBuffer(wbout)], {
    type: 'application/octet-stream'
  });
}
