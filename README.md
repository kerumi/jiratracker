# jiratracker

A tiny logger implementation with JIRA Cloud Services.
To keep track of current issues on determined projects.

### Important Notes

This SPA project targets through a MVP application the time tracking of current logs on a JIRA board.

In order to test this on your current board, you'll need to grant and create an API Token for your current workspace.

Learn how to do it here
https://confluence.atlassian.com/cloud/api-tokens-938839638.html

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn run serve
```

### Compiles and minifies for production

```
yarn run build
```
